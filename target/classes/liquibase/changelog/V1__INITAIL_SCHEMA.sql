create table probe
(
    id uuid not null,
    planet uuid,
    x integer not null,
    y integer not null,
    direction varchar
);
create table planet
(
    id         uuid not null,
    width      integer,
    height     integer
);