# Probe Challenge
### Requisitos
* Java 1.8+
* Kotlin 1.4+
* Spring Boot 2.3+
* Docker e Docker Compose

### Subindo aplicação, aplicação esta rodando na porta 8080

```docker-compose up```

```mvn spring-boot:run```

### Swagger

http://localhost:8080/swagger-ui.html

