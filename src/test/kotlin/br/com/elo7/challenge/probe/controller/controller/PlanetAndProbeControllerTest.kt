package br.com.elo7.challenge.probe.controller.controller

import br.com.elo7.challenge.probe.controller.controller.config.ApplicationTestConfig
import br.com.elo7.challenge.probe.controller.request.LandProbeRequest
import br.com.elo7.challenge.probe.controller.request.ProbeRequest
import br.com.elo7.challenge.probe.domain.Probe
import br.com.elo7.challenge.probe.repository.ProbeRepository
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Before
import org.junit.jupiter.api.Assertions.*
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext


@WebAppConfiguration
@SpringBootTest
@RunWith(SpringRunner::class)
@ContextConfiguration(classes = [ApplicationTestConfig::class])
class PlanetAndProbeControllerTest {

    @Autowired
    lateinit var context: WebApplicationContext

    @Autowired
    lateinit var probeRepository: ProbeRepository

    private lateinit var mockMvc: MockMvc


    @Before
    fun setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build()
    }

    @org.junit.Test
    fun `when given direction N with X = 1 and Y =2 with follow commands LMLMLMLMM should return X = 1 AND Y = 2 with id and planet fill `() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .post("/planet-with-probes")
                .content(asJsonString(createLandProbeRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.jsonPath("$.[0].x").value(1))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[0].y").value(3))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[0].direction").value("N"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").isNotEmpty)
            .andExpect(MockMvcResultMatchers.jsonPath("$.[0].planet").isNotEmpty)
    }


    @org.junit.Test
    fun `should find by planet id`() {
        val created = mockMvc.perform(
            MockMvcRequestBuilders
                .post("/planet-with-probes")
                .content(asJsonString(createLandProbeRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isCreated)
            .andReturn().response.contentAsString.replace("[", "").replace("]", "")

        val createdParsed = ObjectMapper().readValue(created, Probe::class.java)
        val planetId = createdParsed.planet
        val probeFetched = mockMvc.perform(
            MockMvcRequestBuilders
                .get("/planet-with-probes/{planetId}", planetId)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        ).andReturn().response.contentAsString.replace("[", "").replace("]", "")

        val probes = ObjectMapper().readValue(probeFetched, Probe::class.java)
        assertEquals(createdParsed.id, probes.id)
        assertEquals(createdParsed.planet, probes.planet)

    }

fun asJsonString(obj: Any?): String {
    return try {
        ObjectMapper().writeValueAsString(obj)
    } catch (e: Exception) {
        throw RuntimeException(e)
    }
}

    fun createLandProbeRequest() =
        LandProbeRequest(
            width = 5,
            height = 5,
            probes = listOf(
                ProbeRequest(
                    x = 1,
                    y = 2,
                    direction = "N",
                    commands = "LMLMLMLMM"
                )
            )
        )
}