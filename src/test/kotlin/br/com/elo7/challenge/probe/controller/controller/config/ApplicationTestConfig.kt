package br.com.elo7.challenge.probe.controller.controller.config

import br.com.elo7.challenge.Application
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(Application::class)
class ApplicationTestConfig
