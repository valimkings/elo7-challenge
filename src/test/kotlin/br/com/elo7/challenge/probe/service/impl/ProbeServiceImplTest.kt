package br.com.elo7.challenge.probe.service.impl

import br.com.elo7.challenge.error.ExceededPlanetBorder
import br.com.elo7.challenge.probe.controller.controller.config.ApplicationTestConfig
import br.com.elo7.challenge.probe.controller.request.LandProbeRequest
import br.com.elo7.challenge.probe.controller.request.ProbeRequest
import br.com.elo7.challenge.probe.service.ProbeService
import org.junit.Assert.assertThrows
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration


@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = [ApplicationTestConfig::class])
class ProbeServiceImplTest {


    @Autowired
    lateinit var probeService: ProbeService

    @Test
    fun `should return x = 1 and y = 2 when request direction = N and command = LMLMLMLMM`() {
        val created = probeService.create(createLandProbeRequest(x = 1, y = 2, direction = "N", command = "LMLMLMLMM"))
        Assertions.assertEquals(created.first().x, 1)
        Assertions.assertEquals(created.first().y, 3)
        Assertions.assertNotNull(created.first().id)
        Assertions.assertNotNull(created.first().planet)

    }

    @Test
    fun `should throw exception when result commands is more big than planet size`() {
        assertThrows(ExceededPlanetBorder::class.java) {
            probeService.create(
                createLandProbeRequest(
                    width = 4,
                    height = 4,
                    x = 5,
                    y = 5,
                    direction = "N",
                    command = "MMMMMMMM"
                )
            )
        }
    }

    @Test
    fun should_change_probe_direction_from_W_To_S_when_receive_the_command_L() {
        val created = probeService.create(createLandProbeRequest(x = 1, y = 2, direction = "W", command = "L"))
        Assertions.assertEquals("S", created.first().direction)
    }

    @Test
    fun should_change_probe_direction_from_S_To_E_when_receive_the_command_L() {
        val created = probeService.create(createLandProbeRequest(x = 1, y = 2, direction = "S", command = "L"))
        Assertions.assertEquals("E", created.first().direction)
    }

    @Test
    fun should_change_probe_direction_from_E_To_N_when_receive_the_command_L() {
        val created = probeService.create(createLandProbeRequest(x = 1, y = 2, direction = "E", command = "L"))
        Assertions.assertEquals("N", created.first().direction)
    }

    @Test
    fun should_change_probe_direction_from_N_To_E_when_receive_the_command_R() {
        val created = probeService.create(createLandProbeRequest(x = 1, y = 2, direction = "N", command = "R"))
        Assertions.assertEquals("E", created.first().direction)
    }

    @Test
    fun should_change_probe_direction_from_E_To_S_when_receive_the_command_R() {
        val created = probeService.create(createLandProbeRequest(x = 1, y = 2, direction = "E", command = "R"))
        Assertions.assertEquals("S", created.first().direction)
    }

    @Test
    fun should_change_probe_direction_from_S_To_W_when_receive_the_command_R() {
        val created = probeService.create(createLandProbeRequest(x = 1, y = 2, direction = "S", command = "R"))
        Assertions.assertEquals("W", created.first().direction)
    }

    @Test
    fun should_change_probe_direction_from_W_To_N_when_receive_the_command_R() {
        val created = probeService.create(createLandProbeRequest(x = 1, y = 2, direction = "W", command = "R"))
        Assertions.assertEquals("N", created.first().direction)
    }

    @Test
    fun should_change_probe_position_from_1_1_N_To_1_2_N_when_receive_the_command_M() {
        val created = probeService.create(createLandProbeRequest(x = 1, y = 1, direction = "N", command = "M"))
        Assertions.assertEquals("N", created.first().direction)
    }

    @Test
    fun should_change_probe_position_from_1_1_S_To_1_0_S_when_receive_the_command_M() {

        val created = probeService.create(createLandProbeRequest(x = 1, y = 1, direction = "S", command = "M"))
        Assertions.assertEquals("S", created.first().direction)

    }

    @Test
    fun should_change_probe_position_from_1_1_W_To_0_1_W_when_receive_the_command_M() {
        val created = probeService.create(createLandProbeRequest(x = 1, y = 1, direction = "W", command = "M"))
        Assertions.assertEquals("W", created.first().direction)

    }

    @Test
    fun should_change_probe_position_from_1_1_E_To_2_1_E_when_receive_the_command_M() {
        val created = probeService.create(createLandProbeRequest(x = 1, y = 1, direction = "E", command = "M"))
        Assertions.assertEquals("E", created.first().direction)
    }

    fun createLandProbeRequest(width: Int = 5, height: Int = 5, x: Int, y: Int, direction: String, command: String) =
        LandProbeRequest(
            width = 5,
            height = 5,
            probes = listOf(
                ProbeRequest(
                    x = x,
                    y = y,
                    direction = direction,
                    commands = command
                )
            )
        )
}