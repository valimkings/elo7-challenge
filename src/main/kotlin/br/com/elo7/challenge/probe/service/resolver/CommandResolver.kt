package br.com.elo7.challenge.probe.service.resolver

import br.com.elo7.challenge.probe.domain.Probe
import java.lang.String

enum class CommandResolver {
    R {
        override fun execute(probe: Probe):Probe {
            return MoveResolverRight.valueOf(probe.direction).execute(probe)
        }
    },
    L {
        override fun execute(probe: Probe):Probe {
            return MoveResolverLeft.valueOf(probe.direction).execute(probe)
        }
    },
    M {
        override fun execute(probe: Probe):Probe {
            return MoveResolverFoward.valueOf(probe.direction).execute(probe)
        }
    };
    abstract fun execute(probe: Probe) : Probe
}
