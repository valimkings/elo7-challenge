package br.com.elo7.challenge.probe.service.resolver

import br.com.elo7.challenge.probe.domain.Direction
import br.com.elo7.challenge.probe.domain.Probe

enum class MoveResolverRight {
    N {
        override fun execute(probe: Probe): Probe {
            probe.direction = Direction.E.toString()
            return probe
        }
    },
    W {
        override fun execute(probe: Probe):Probe {
            probe.direction = Direction.N.toString()
            return probe
        }
    },
    E {
        override fun execute(probe: Probe):Probe {
            probe.direction = Direction.S.toString()
            return probe
        }
    },
    S {
        override fun execute(probe: Probe):Probe {
            probe.direction = Direction.W.toString()
            return probe
        }
    };

    abstract fun execute(probe: Probe): Probe
}

