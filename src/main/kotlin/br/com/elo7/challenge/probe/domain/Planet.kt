package br.com.elo7.challenge.probe.domain

import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Planet(
    @Id
    var id: UUID? = null,
    var width: Int,
    var height: Int,
)