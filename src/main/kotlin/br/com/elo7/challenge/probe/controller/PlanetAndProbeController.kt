package br.com.elo7.challenge.probe.controller

import br.com.elo7.challenge.probe.controller.request.LandProbeRequest
import br.com.elo7.challenge.probe.domain.Probe
import br.com.elo7.challenge.probe.service.ProbeService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/planet-with-probes")
@Api(value="Proble Challenge")
@CrossOrigin(origins = ["*"])
class PlanetAndProbeController(
    private val probeService: ProbeService,
) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Cria planet com seus respectivos Probes")
    fun create(@RequestBody @Valid landProbeRequest: LandProbeRequest): List<Probe>  {
        return probeService.create(landProbeRequest)
    }

    @GetMapping("/{planetId}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Retorna uma lista de probes pelo id do planet")
    fun findById(@PathVariable planetId: String): List<Probe> {
        return probeService.findAllByPlanetId(planetId)
    }

}