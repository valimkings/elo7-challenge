package br.com.elo7.challenge.probe.domain

import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Probe(
    @Id
    var id: UUID? = null,
    var x: Int,
    var y: Int,
    var direction: String,
    var planet: UUID? = null

)