package br.com.elo7.challenge.probe.service.resolver

import br.com.elo7.challenge.probe.domain.Probe


enum class MoveResolverFoward {
    N {
        override fun execute(probe: Probe): Probe {
            probe.x = probe.x
            probe.y = probe.y + 1
            return probe
        }
    },
    W {
        override fun execute(probe: Probe): Probe {
            probe.x = probe.x - 1
            probe.y = probe.y
            return probe
        }
    },
    E {
        override fun execute(probe: Probe): Probe {
            probe.x = probe.x + 1
            probe.y = probe.y
            return probe
        }
    },
    S {
        override fun execute(probe: Probe): Probe {
            probe.x = probe.x
            probe.y = probe.y - 1
            return probe
        }
    };

    abstract fun execute(probe: Probe): Probe
}
