package br.com.elo7.challenge.probe.service

import br.com.elo7.challenge.probe.controller.request.LandProbeRequest
import br.com.elo7.challenge.probe.domain.Probe

interface ProbeService {
    fun create(landProbeRequest: LandProbeRequest) : List<Probe>
    fun findAllByPlanetId(planetId: String): List<Probe>
}