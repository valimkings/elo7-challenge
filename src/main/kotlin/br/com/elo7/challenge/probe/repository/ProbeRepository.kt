package br.com.elo7.challenge.probe.repository

import br.com.elo7.challenge.probe.domain.Probe
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*


@Repository
interface ProbeRepository : JpaRepository<Probe, UUID>{
    fun findAllByPlanet(planetId: UUID): List<Probe>
}