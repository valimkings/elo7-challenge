package br.com.elo7.challenge.probe.service.impl

import br.com.elo7.challenge.config.logger
import br.com.elo7.challenge.error.CommandNotFound
import br.com.elo7.challenge.error.ExceededPlanetBorder
import br.com.elo7.challenge.probe.controller.mapper.Mapper
import br.com.elo7.challenge.probe.controller.request.LandProbeRequest
import br.com.elo7.challenge.probe.controller.request.ProbeRequest
import br.com.elo7.challenge.probe.domain.Planet
import br.com.elo7.challenge.probe.domain.Probe
import br.com.elo7.challenge.probe.repository.PlanetProbeRepository
import br.com.elo7.challenge.probe.repository.ProbeRepository
import br.com.elo7.challenge.probe.service.ProbeService
import br.com.elo7.challenge.probe.service.resolver.CommandResolver
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import java.util.function.Consumer
import java.util.stream.Collectors

@Service
class ProbeServiceImpl(
    private val probeRepository: ProbeRepository,
    private val planetProbeRepository: PlanetProbeRepository
) : ProbeService {

    @Transactional
    override fun create(landProbeRequest: LandProbeRequest): List<Probe> {
        val planet = Mapper.mapToPlanetFromRequest(landProbeRequest)
        planetProbeRepository.save(planet)
        logger().info("success on save planet with id-> ${planet.id}")

        val convertedProbes = convertAndMoveProbes(landProbeRequest, planet)
        validatePlanetSize(convertedProbes, landProbeRequest)
        convertedProbes.forEach(Consumer { probe: Probe -> probeRepository.save(probe) })
        logger().info("success on process all probes")

        return convertedProbes
    }

    private fun validatePlanetSize(convertedProbes: List<Probe>, landProbeRequest: LandProbeRequest) {
        convertedProbes.firstOrNull { it.x > landProbeRequest.height || it.y > landProbeRequest.width }
            .takeIf { it != null }?.let {
                throw ExceededPlanetBorder("One or more probes exceeds the size of the planet")
            }
    }

    override fun findAllByPlanetId(planetId: String): List<Probe> =
        probeRepository.findAllByPlanet(UUID.fromString(planetId))

    private fun convertAndMoveProbes(landProbeRequest: LandProbeRequest, planet: Planet): List<Probe> {
        return landProbeRequest.probes
            .stream().map { probeRequest ->
                val probe = Mapper.mapToProbeFromRequest(probeRequest, planet)
                moveProbeWithAllCommands(probe, probeRequest)
                probe
            }.collect(Collectors.toList())
    }

    private fun moveProbeWithAllCommands(probe: Probe, probeRequest: ProbeRequest)  {
        probeRequest.commands.toCharArray().forEach { command ->
            applyCommandToProbe(probe, command)
        }
    }

    private fun applyCommandToProbe(probe: Probe, command: Char) {
        try {
            CommandResolver.valueOf(command.toString()).execute(probe)
        } catch (_: Exception){
            logger().error("Command not found, error on apply command")
            throw CommandNotFound("Command -> $command not found, error on apply command")
        }
    }
}
