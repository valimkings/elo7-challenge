package br.com.elo7.challenge.probe.domain

object Direction {
    const val N = 'N'
    const val W = 'W'
    const val E = 'E'
    const val S = 'S'
}