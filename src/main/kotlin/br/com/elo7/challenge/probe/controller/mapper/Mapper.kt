package br.com.elo7.challenge.probe.controller.mapper

import br.com.elo7.challenge.probe.controller.request.LandProbeRequest
import br.com.elo7.challenge.probe.controller.request.ProbeRequest
import br.com.elo7.challenge.probe.domain.Planet
import br.com.elo7.challenge.probe.domain.Probe
import java.util.*

object Mapper {

    fun mapToPlanetFromRequest(landProbeRequest: LandProbeRequest) = Planet(
            id = UUID.randomUUID(),
            height = landProbeRequest.height,
            width = landProbeRequest.width
    )

    fun mapToProbeFromRequest(probeRequest: ProbeRequest, planet: Planet): Probe {
        return Probe(
            id = UUID.randomUUID(),
            planet = planet.id,
            x = probeRequest.x,
            y = probeRequest.y,
            direction = probeRequest.direction
        )
    }

}