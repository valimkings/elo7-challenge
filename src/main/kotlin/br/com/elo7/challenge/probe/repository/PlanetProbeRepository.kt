package br.com.elo7.challenge.probe.repository

import br.com.elo7.challenge.probe.domain.Planet
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*
@Repository
interface PlanetProbeRepository : JpaRepository<Planet, UUID>