package br.com.elo7.challenge.probe.controller.request

import com.sun.istack.NotNull
import javax.validation.constraints.NotEmpty
import kotlin.collections.List

data class LandProbeRequest(
    @field:NotNull
    val width: Int,
    @field:NotNull
    val height: Int,
    @field:NotEmpty
    val probes: List<ProbeRequest>
)

data class ProbeRequest(
    val x: Int,
    val y: Int,
    val direction: String,
    val commands: String
)

