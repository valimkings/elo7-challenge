package br.com.elo7.challenge.error

class CommandNotFound(message: String) : RuntimeException(message)