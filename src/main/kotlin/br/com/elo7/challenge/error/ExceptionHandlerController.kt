package br.com.elo7.challenge.error

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.server.ResponseStatusException

@ControllerAdvice
class ExceptionHandlerController {

    @ExceptionHandler(CommandNotFound::class)
    fun handleConflictException(e: CommandNotFound): HttpStatus {
        throw ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.message, e)
    }
    @ExceptionHandler(ExceededPlanetBorder::class)
    fun handleConflictException(e: ExceededPlanetBorder): HttpStatus {
        throw ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.message, e)
    }

}
