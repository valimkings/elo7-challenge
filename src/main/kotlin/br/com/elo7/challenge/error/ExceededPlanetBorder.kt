package br.com.elo7.challenge.error

class ExceededPlanetBorder(message: String) : RuntimeException(message)