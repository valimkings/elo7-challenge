package br.com.elo7.challenge

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration
import springfox.documentation.swagger2.annotations.EnableSwagger2

@SpringBootApplication
@Configuration
class Application

fun main(args: Array<String>) {
	runApplication<Application>(*args)
}
